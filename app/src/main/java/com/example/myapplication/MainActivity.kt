package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var resultTW: TextView

    private var operand: Double = 0.0
    private var operation: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        resultTW = findViewById((R.id.resultTW))

    }

    fun numberClick(clickedView: View) {

        if (clickedView is TextView) {

            var result = resultTW.text.toString()
            val number = clickedView.text.toString()

            if (result == "0") {
                result = ""
            }

            resultTW.text = result + number

        }

    }

    fun operationClick(clickedView: View) {

        if (clickedView is TextView) {

            val result = resultTW.text.toString()

            if (result.isNotEmpty()) {
                operand = result.toDouble()
            }

            operation = clickedView.text.toString()

            resultTW.text = ""

        }

    }

    fun equalsClick(clickedView: View) {

        val secOperandText = resultTW.text.toString()
        var secOperand: Double = 0.0

        if (secOperandText.isNotEmpty()) {
            secOperand = secOperandText.toDouble()
        }

        when (operation) {
            "+" -> resultTW.text = (operand + secOperand).toString()
            "-" -> resultTW.text = (operand - secOperand).toString()
            "*" -> resultTW.text = (operand * secOperand).toString()
            "/" -> resultTW.text = (operand / secOperand).toString()
        }

    }
    fun clearClick(clickedView: View) {
        resultTW.text = ""
    }

    fun clearOneNumberClick(clickedView: View) {
        resultTW.text = resultTW.text.toString().dropLast(1)
    }
}




